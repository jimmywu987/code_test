import React from 'react'
import styles from "../css/ImageWithWords.module.css";
export default function ImageWithWords({src, title, words}:{src:string,title:string, words:string}) {
    return (
        <div className={styles.container}>
            <div style={{"marginBottom": "20px"}}><img src={src} alt="pic" /></div>
            <div style={{"alignSelf": "center"}}>
            <div className="w-20" style={{"fontWeight": "bold", "color": "#333333", "width": "200px" }}>{title}</div>
            <div className="w-75" style={{"color": "#595F5E" , "maxWidth": "370px", "alignSelf" : "flex-end"}}>{words}</div>
            </div>
        </div>
    )
}
