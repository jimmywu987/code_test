import React from 'react'
import styles from "../css/Nav.module.css";
export default function Nav() {
    return (
        <>
        <div className={styles.mobile}>
                <div className={styles.logo_mobile}><img src="Hamburger_Menu.png" alt="logo"/></div>
                <div className={styles.logo}> <img src="123.png" alt="logo"/></div>
                <div className={styles.logo_mobile}><img src="Search.png" alt="logo"/></div>
            </div>
        <div className={styles.nav}>
            <div className={styles.left_to_logo} >
            <div className={styles.logo}>
                <img src="123.png" alt="logo"/>
            </div>
            <div className={styles.left}>
                <div>關於</div>
                <div>工作</div>
                <div>FREELANCER<span>S</span></div>
                <div>合作</div>
                <button>
                升級計劃
                </button>
            </div>
            </div>
            <div className={styles.right}>
                <div className={styles.right_logo}><img src="Search.png" alt="search logo"/></div>
                <div className={styles.right_logo}><img src="Message.png" alt="message logo"/></div>
                <div className={styles.right_logo}><img src="Bell.png" alt="bell logo"/></div>
                <div className={styles.avatar}><img src="Avatar.png" alt="persion"/></div>
                <div className={styles.word}>中(港)</div>
            </div>
        </div>
        </>
    )
}
