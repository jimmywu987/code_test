import React from 'react'
import styles from "../css/ContentLowerPart.module.css";
import ImageWithWords from './ImageWithWords';
export default function ContentLowerPart() {

    return (
        <div className={styles.lower}>
            <div className={styles.title}>還有更多福利</div>
            <div className={styles.image_word}>
                <ImageWithWords src="4.png" title="申請工作時更顯眼" words="每一個專頁廣告用戶都會獲得「推薦徽章」，在申請工作室更能夠讓你在芸芸申請者中脫穎而出！"/>
                <ImageWithWords src="5.png" title="社交媒體推廣" words="我們每月會精選數位專頁廣告用戶，並會在我們官方社交平台上推廣它們的作品。"/>
            </div>
        </div>
    )
}
