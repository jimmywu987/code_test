import React from 'react'
import styles from "../css/ContentBottom.module.css";
export default function ContentBottom() {
    return (
        <div className={styles.upper_content}>
            <img src="background1.png" alt="background" />
            <div className={styles.word}>
            <div className={styles.upper_word}>立即開始</div>
            <button className={styles.button}><span className={styles.button_word}>馬上選擇計劃</span ><span className={styles.dir}>&gt;</span></button>
        </div>
        </div>
    )
}
