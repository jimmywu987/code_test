import React from 'react'
import styles from "../css/AnswerQuestionEach.module.css";
export default function AnswerQuestionEach({title, sentence1, sentence2, sentence3}: {title:string,sentence1: string, sentence2:string | "", sentence3:string | ""}) {
    let orangeKeyword = `專頁廣告`
    let titleArr = title.split("-")


    return (
        <div className={styles.word} style={{"width": "700px", "margin": "0 10px"}}>
            <div className={styles.flex}><div className={styles.image}><img src="questionmark.png" alt="icon" /></div>{titleArr[0]}<span className={styles.keyword}>{orangeKeyword}</span>{titleArr[1]}</div>
            <div className={styles.sentence}>{sentence1}</div>
            <div className={styles.sentence}>{sentence2}</div>
           <div className={styles.sentence}>{sentence3}</div>
        </div>
    )
}
