import React from 'react'
import styles from "../css/ContentLowestPart.module.css";
import AnswerQuestionEach from './AnswerQuestionEach';
export default function ContentLowestPart() {

    return (
        <div className={styles.lower}>
            <div className={styles.title}>有疑問？</div>
            <div className={styles.word}>我們會為你一一解答，以下是我們經常收到的問題。</div>
            <div style={{"marginTop": "60px"}}>
                <div className={styles.row}>
                    <AnswerQuestionEach title="甚麼是-?" sentence1={`專頁廣告是助你推廣個人品牌的工具。`} sentence2={`購買專頁廣告後，你的專頁將會優先以特別設計出現於網站主頁、搜尋頁面、相關行業文章以及客戶工作記錄版。專頁更   顯眼突出，自然增加曝光率及合作邀請機會。`} sentence3={`一般來說，在沒有購買專頁廣告的情況下，個人專頁會根    據其作品質素及數量等等因素，按排名出現於搜尋頁面，但購買了專頁廣告的用戶，該個人專頁將自動在搜尋頁面被優先  置頂。`} />
                    <AnswerQuestionEach title="-是如何運作的？" sentence1={`專頁廣告會於購買後立即自動生效。當曝光次數   被消費至計劃上限的80%，Freehunter會向用戶發出關於自動續約的提示電郵。`} sentence2={`當計劃的曝光次數全   被消費，除非收到用戶的取消訂閱指令，系統將自動續約。續約收費以對上一次購買的計劃作準。`} sentence3="" />
                    
                    
                </div>
                <div className={styles.row}>
                <AnswerQuestionEach title="我有多於一個專頁，該如何購買-?" sentence1={`若你擁有多於一個專頁，於購買  時，你可指定選擇你想推廣的專頁。你亦可一次選擇多個專頁，收費將會按照被選取的專頁數量計算。`}   sentence2="" sentence3=""/>
                <AnswerQuestionEach title="如何查看-表現？" sentence1={`若想查詢專頁廣告的數據，可以到「我的賬戶」的「專頁數據」 查看相關資料。數據版上會顯示不同數據與圖表，例如點擊率、瀏覽者逗留時間等等。`}   sentence2="" sentence3=""/>
                   
                </div>
                <div className={styles.row}>
                <AnswerQuestionEach title="如何更改/取消-？" sentence1={`如果你希望取消專頁廣告訂閱，可以到「我的賬  戶」裡的「付費項目」，並點擊「取消按鈕」。`} sentence2={`若想更改專頁廣告計劃，你需要先取消現有計劃訂閱 後，然後再次來到這頁面購買理想的專頁廣告計劃。`} sentence3=""/>
                <AnswerQuestionEach title="購買-後，可以要求退款嗎？" sentence1={`由於專頁廣告會於購買後立即自動生   效，因此「專頁廣告」在任何情況下均不設退款。`} sentence2={`作為提醒，當曝光次數被消費至計劃上限的80%，我 們會向你發出關於自動續約的通知。如果你不希望續約，請在自動收費前取消訂閱。`} sentence3=""/>
                    
                </div>
            </div>
        </div>
    )
}
