import React from 'react'
import styles from "../css/ContentUpperPart.module.css";
export default function ContentUpperPart() {
    return (
        <>
        <div className={styles.upper_content}>
            <img src="background1.png" alt="background" />
            <div className={styles.word}>
        <div className={styles.upper_word}>獲得更多曝光次數，優先展示你的專頁。</div>
        <div className={styles.lower_word}>進一步推廣個人品牌，吸引客戶主動聯絡。</div>
        </div>
        </div>
    </>
    )
}
