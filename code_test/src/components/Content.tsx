import React from 'react'
import styles from "../css/Content.module.css";
import EachSentence from './EachSentence';
interface Props {
    flex: "row" | "row-reverse",
    img:string,
    title:string,
    src1:string,
    bigImgWeigth:string,
    smallTitle1:string,
    words1:string,src2:string,
    smallTitle2:string,
    words2:string,
    src3:string,
    smallTitle3:string,
    words3:string,
    width1: string,
    width2: string,
    width3: string
}
export default function Content({flex, img, bigImgWeigth, title, src1, smallTitle1, words1,src2, smallTitle2, words2,src3, smallTitle3, words3, width1, width2, width3  }: Props ) {
    return (
           <>
           <div className={styles.container} style={{"flexDirection": flex }}>
               <div>
            <div className={styles.title} style={{"marginBottom": "20px"}}>{title}</div>
                    <EachSentence src={src1} smallTitle={smallTitle1} words={words1} width={width1}/>
                    <EachSentence src={src2} smallTitle={smallTitle2} words={words2} width={width2}/>
                    <EachSentence src={src3} smallTitle={smallTitle3} words={words3} width={width3}/>
               </div>
               <div className={styles.top} style={{"maxWidth": bigImgWeigth}}>
                   <img src={img} alt="pic-1" />
               </div>
           </div>
           </>
    )
}

