import React from 'react'
import styles from "../css/EachSentence.module.css";
export default function EachSentence({src, smallTitle, words, width} : {src: string, smallTitle: string, words:string, width:string}) {
    return (
        <div className={styles.section}>
            <div className={`w-30 image`} style={{"width": width}}>
                <img src={`${src}`} alt="icon" />
            </div>
            <div>
                <div className="w-20" style={{"fontWeight": "bold", "color": "#333333"}}>{smallTitle}</div>
                <div className="w-75" style={{"color": "#595F5E"}}>{words}</div>
            </div>
        </div>
    )
}
